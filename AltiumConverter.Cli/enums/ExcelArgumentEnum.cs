﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumConverter.Cli.enums
{
    internal enum ExcelArgumentEnum
    {
        Ok,
        LengthError,
        UnknownName,
        IncompleteArguments,
        InFileNotExists,
        OutFileNamingError,
        ParseError,
        ExtensionError
    }
}
