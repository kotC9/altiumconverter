﻿using AltiumConverter.Cli.enums;
using AltiumConverter.Cli.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumConverter.Cli.validators;
using AltiumConverter.Core.models;

namespace AltiumConverter.Cli.@abstract
{
    internal abstract class AbstractArgumentParser
    {
        public abstract IParseResult<ExcelArgumentEnum, InputArgumentsModel> Parse(string[] data);
    }
}
