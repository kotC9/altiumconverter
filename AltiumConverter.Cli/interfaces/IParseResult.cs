﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumConverter.Cli.interfaces
{
    internal interface IParseResult<T, TModel>
    {
        T GetStatus();
        TModel? GetModel();
        string GetMessage();
    }
}
