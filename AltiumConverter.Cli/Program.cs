﻿using AltiumConverter.Cli.enums;
using AltiumConverter.Cli.validators;
using AltiumConverter.Core.models;
using Microsoft.Extensions.Logging;

namespace AltiumConverter.Cli
{

    internal partial class Program
    {

        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                args = new[] { "--input=in.xls", "--output=out.xls" };
            }
            using ILoggerFactory factory = LoggerFactory.Create(builder => builder.AddConsole());
            ILogger programLogger = factory.CreateLogger("Program");

            var parser = new ArgumentsParser();

            var parseResult = parser.Parse(args);

            if (parseResult.GetStatus() != ExcelArgumentEnum.Ok)
            {
                programLogger.Log(LogLevel.Error, parseResult.GetMessage());
                return;
            }

            var converter = new Core.converter.AltiumConverter();


            var (ok, invalidList) = converter.TryConvert(parseResult.GetModel()!);
            
            Console.WriteLine(!ok ? "Не удалось создать документ" : "Документ создан");
            if (ok) return;

            bool isAnswerYes(string ans)
            {
                return ans == "" || ans.ToLower() == "y" || ans.ToLower() == "д";
            }

            Console.WriteLine($"Не удалось сконвертировать файл {parseResult.GetModel()!.InPath}");

            if (invalidList.Count <= 0) return;

            Console.Write($"{invalidList.Count} строк из входного файла не распознаны, занести в базу данных? [Д/н] ");

            if (!isAnswerYes(Console.ReadLine()!)) return;
            
            Console.WriteLine($"Для выхода нажмите Ctrl+C");
            var i = 1;
            foreach (var info in invalidList)
            {
                var lineValid = false;
                var elementType = "";
                var elementName = "";
                do
                {
                    Console.WriteLine($"[{i}/{invalidList.Count}] Строка: {info.Comment} | {info.Description} | {info.Designator} | {info.Footprint} | {info.LibRef}");
                    Console.Write("Введите тип элемента: ");
                    elementType = Console.ReadLine()!;
                    Console.Write("Введите название элемента: ");
                    elementName = Console.ReadLine()!;

                    Console.Write("Данные верны? [Д/н]");
                    lineValid = isAnswerYes(Console.ReadLine()!);
                } while (!lineValid);

                converter.AddComponent(new ComponentModel(info, elementType, elementName));

                i++;
            }

            Console.WriteLine("Повторная попытка создать документ");

            (ok, _) = converter.TryConvert(parseResult.GetModel()!);

            Console.WriteLine(!ok ? "Не удалось создать документ" : "Документ создан");
        }
    }
}