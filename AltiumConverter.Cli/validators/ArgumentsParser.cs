﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using AltiumConverter.Cli.@abstract;
using AltiumConverter.Cli.enums;
using AltiumConverter.Cli.interfaces;
using AltiumConverter.Core.models;

namespace AltiumConverter.Cli.validators
{


    internal class ArgumentsParser : AbstractArgumentParser
    {
        private const string CorrectExtension = ".xls";

        public override IParseResult<ExcelArgumentEnum, InputArgumentsModel> Parse(string[] data)
        {
            if (data.Length == 1 && InputArgumentsModel.ToCommandType(data[0]) == CommandType.Help)
            {
                return new ArgumentParseResult(ExcelArgumentEnum.Ok);
            }

            if (data.Length != 2)
            {
                return new ArgumentParseResult(ExcelArgumentEnum.LengthError,
                    $"Количество аргументов({data.Length}) не соответствует (ожидается 2 аргумента)");
            }

            var argumentModel = new InputArgumentsModel();

            foreach (var arg in data)
            {
                try
                {
                    var commandParsed = arg.Split('=');
                    var commandName = commandParsed[0];
                    var commandContent = commandParsed[1];

                    if (!argumentModel.TrySetProperty(commandName, commandContent))
                    {
                        return new ArgumentParseResult(ExcelArgumentEnum.UnknownName,
                            $"Неизвестный аргумент {commandName}");
                    }
                }
                catch
                {
                    return new ArgumentParseResult(ExcelArgumentEnum.ParseError, "Ошибка во время парсинга аргументов");
                }
            }

            if (!File.Exists(argumentModel.InPath))
            {
                return new ArgumentParseResult(ExcelArgumentEnum.InFileNotExists, "Входной файл не существует");
            }

            if (Path.GetExtension(argumentModel.InPath) != CorrectExtension)
            {
                return new ArgumentParseResult(ExcelArgumentEnum.ExtensionError, $"Расширение входного файла не соответствует нужному {CorrectExtension}");
            }

            return new ArgumentParseResult(ExcelArgumentEnum.Ok, "", argumentModel);
        }
    }
}
