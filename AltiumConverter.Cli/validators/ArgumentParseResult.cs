﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumConverter.Cli.enums;
using AltiumConverter.Cli.interfaces;
using AltiumConverter.Core.models;

namespace AltiumConverter.Cli.validators
{
    internal class ArgumentParseResult : IParseResult<ExcelArgumentEnum, InputArgumentsModel>
    {
        public ExcelArgumentEnum Result { get; }
        public ExcelArgumentEnum Status { get; set; }
        public InputArgumentsModel? Model { get; set; }
        public string Message { get; set; }

        public ArgumentParseResult(ExcelArgumentEnum result, string message = "", InputArgumentsModel? model = null)
        {
            Result = result;
            Message = message;
            Model = model;
        }

        public ExcelArgumentEnum GetStatus()
        {
            return Status;
        }

        public InputArgumentsModel? GetModel()
        {
            return Model;
        }

        public string GetMessage()
        {
            return Message;
        }
    }
}
