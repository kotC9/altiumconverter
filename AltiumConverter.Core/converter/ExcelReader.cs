﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumConverter.Core.models;
using Aspose.Cells;
using Range = System.Range;

namespace AltiumConverter.Core.converter
{
    public class ExcelReader
    {
        private readonly string _modelInPath;
        private readonly (string, int) _comment;
        private readonly (string, int)_description;
        private readonly (string, int)_designator;
        private readonly (string, int)_footprint;
        private readonly (string, int)_libref;
        private readonly (string, int)_quanity;

        public ExcelReader(string modelInPath, string comment, string description, string designator, string footprint, string libref, string quanity)
        {
            _modelInPath = modelInPath;
            _comment = (comment, 0);
            _description = (description, 1);
            _designator = (designator, 2);
            _footprint = (footprint, 3);
            _libref = (libref, 4);
            _quanity = (quanity, 5);
        }

        public List<ComponentRawInfo> ReadContent()
        {
            var wb = new Workbook(_modelInPath);

            var collection = wb.Worksheets;
            var worksheet = collection[0];

            var rows = worksheet.Cells.MaxDataRow;
            var cols = worksheet.Cells.MaxDataColumn;

            var res = new List<ComponentRawInfo>();

            for (var i = 1; i < rows; i++)
            {
                res.Add(new ComponentRawInfo(
                    worksheet.Cells[i, _comment.Item2].Value.ToString()!,
                    worksheet.Cells[i, _description.Item2].Value.ToString()!,
                    worksheet.Cells[i, _designator.Item2].Value.ToString()!,
                    worksheet.Cells[i, _footprint.Item2].Value.ToString()!,
                    worksheet.Cells[i, _libref.Item2].Value.ToString()!,
                    worksheet.Cells[i, _quanity.Item2].Value.ToString()!));
            }

            return res;
        }
    }
}
