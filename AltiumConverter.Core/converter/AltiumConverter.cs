﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumConverter.Core.db;
using AltiumConverter.Core.models;
namespace AltiumConverter.Core.converter
{
    public class AltiumConverter
    {
        private readonly DatabaseWorker _databaseWorker = new();

        public void AddComponent(ComponentModel model)
        {
            _databaseWorker.Add(model);
        }

        public (bool, List<ComponentRawInfo>) TryConvert(InputArgumentsModel model)
        {
            var excelReader = new ExcelReader(model.InPath,
                "Comment", "Description", "Designator", "Footprint", "LibRef", "Quanity");

            var lines = excelReader.ReadContent();

            var invalidInfoList = lines.Where(info => !ValidInfo(info)).ToList();

            if (invalidInfoList.Count > 0)
            {
                return (false, invalidInfoList);
            }

            var gostGenerator = new GostGenerator(_databaseWorker, model.OutPath);

            gostGenerator.Generate(lines);
            gostGenerator.Save();

            return (true, invalidInfoList);
        }

        private bool ValidInfo(ComponentRawInfo info)
        {
            return _databaseWorker.HasInfo(info);
        }
    }
}
