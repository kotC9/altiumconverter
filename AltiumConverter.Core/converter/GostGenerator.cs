﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using AltiumConverter.Core.db;
using AltiumConverter.Core.models;
using Aspose.Cells;
using SaveOptions = Aspose.Cells.SaveOptions;

namespace AltiumConverter.Core.converter
{
    class WorksheetInfo
    {
        private int _currentRow = 0;
        public int StartRow { get; set; }
        public Worksheet Worksheet { get; set; }
        public int AvailableLines { get; set; }
        public int ColumnDesignator { get; set; }
        public int ColumnName { get; set; }
        public int ColumnCount { get; set; }
        public int ColumntComment { get; set; }

        public bool TryAddType(string type)
        {
            if (_currentRow == 0)
                _currentRow = StartRow;

            const int needLines = 2;

            if (AvailableLines + StartRow - _currentRow < needLines)
                return false;

            Worksheet.Cells[_currentRow, ColumnName].PutValue(type);
            Worksheet.Cells[_currentRow, ColumnName].Characters(0, type.Length).Font.Underline =
                FontUnderlineType.Single;

            _currentRow+=needLines;
            return true;
        }

        public bool TryAddElement(ComponentGostModel element)
        {
            if (_currentRow == 0)
                _currentRow = StartRow;

            const int needLines = 1;

            if (AvailableLines + StartRow - _currentRow < needLines)
                return false;
            
            
            Worksheet.Cells[_currentRow, ColumnDesignator].PutValue(element.Designator);
            if (element.Designator.Length > 9)
            {
                Worksheet.Cells[_currentRow, ColumnDesignator].Characters(0, element.Designator.Length).Font.Size = 10;
                Worksheet.Cells.SetRowHeight(_currentRow, 29);
            }

            var nameStyle = Worksheet.Cells[_currentRow, ColumnName].GetStyle();
            nameStyle.HorizontalAlignment = TextAlignmentType.Left;
            Worksheet.Cells[_currentRow, ColumnName].SetStyle(nameStyle);
            Worksheet.Cells[_currentRow, ColumnName].PutValue(element.GostName);

            Worksheet.Cells[_currentRow, ColumnCount].PutValue(element.Quanity);
            Worksheet.Cells[_currentRow, ColumntComment].PutValue("");

            _currentRow+=needLines;
            return true;
        }

        public bool TryAddSpace()
        {
            if (_currentRow == 0)
                _currentRow = StartRow;

            const int needLines = 1;
            
            if (AvailableLines + StartRow - _currentRow < needLines)
                return false;

            _currentRow+=needLines;
            return true;
        }
    }
    internal class GostGenerator
    {
        private const int MainPageLines = 27;
        private const int DefaultPageLines = 31;
        private const int StartRow = 1;
        private const int StartColumn = 1;

        private readonly DatabaseWorker _databaseWorker;
        private readonly string _modelOutPath;

        public GostGenerator(DatabaseWorker databaseWorker, string modelOutPath)
        {
            _databaseWorker = databaseWorker;
            _modelOutPath = modelOutPath;
        }

        public void Generate(List<ComponentRawInfo> rawModels)
        {
            var gostListModels = new Dictionary<string, List<ComponentGostModel>>();

            foreach (var model in _databaseWorker.GetModelsGost(rawModels))
            {
                if (gostListModels.ContainsKey(model.GostType))
                    gostListModels[model.GostType].Add(model);
                else
                    gostListModels.Add(model.GostType, new List<ComponentGostModel>{model});
            }

            var totalLines = 0;
            for(var i = 0; i < gostListModels.Count; i++)
            {
                totalLines += 2;          // название элементов 
                totalLines += gostListModels.ElementAt(i).Value.Count; // элементы

                if (i != gostListModels.Count - 1)
                {
                    totalLines++;
                }
            }

            var defaultPagesCount = totalLines > MainPageLines ? (int)Math.Ceiling((totalLines - MainPageLines) / (double)DefaultPageLines) : 0;

            var wbOut = new Workbook();
            for (var i = 0; i < defaultPagesCount; i++)
            {
                wbOut.Worksheets.Add((i+2).ToString());
            }
            var wbMain = new Workbook("data/template_main.xlsx");
            var wbPage = new Workbook("data/template_page.xlsx");

            CopyWorksheet(wbMain.Worksheets[0], wbOut.Worksheets[0]);
            
            wbOut.Worksheets[0].Cells["L34"].PutValue("1");
            wbOut.Worksheets[0].Cells["M34"].PutValue(defaultPagesCount + 1);

            for (var i = 0; i < defaultPagesCount; i++)
            {
                CopyWorksheet(wbPage.Worksheets[0], wbOut.Worksheets[i+1]);
                wbOut.Worksheets[i + 1].Cells["K35"].PutValue((i+2).ToString());
            }

            var infos = new List<WorksheetInfo>();
            infos.Add(new WorksheetInfo{Worksheet = wbOut.Worksheets[0], AvailableLines = MainPageLines, StartRow = 2, ColumnDesignator = 1, ColumnName=4, ColumnCount=9, ColumntComment=10});
            for (var i = 1; i < wbOut.Worksheets.Count; i++)
            {
                infos.Add(new WorksheetInfo{Worksheet = wbOut.Worksheets[i], AvailableLines = DefaultPageLines, StartRow = 2, ColumnDesignator = 1, ColumnName=4, ColumnCount=8, ColumntComment=9});
            }

            var indexInfo = 0;
            foreach (var (type, elements) in gostListModels)
            {
                if (!infos[indexInfo].TryAddType(type))
                {
                    indexInfo++;
                    infos[indexInfo].TryAddType(type);
                }
                foreach (var element in elements)
                {
                    if (!infos[indexInfo].TryAddElement(element))
                    {
                        indexInfo++;
                        infos[indexInfo].TryAddElement(element);
                    }
                }

                if (!infos[indexInfo].TryAddSpace())
                    indexInfo++;
            }

            FontConfigs.SetFontFolder("data", false);

            wbOut.Save(_modelOutPath);

            wbOut.Save($"{_modelOutPath}.pdf", SaveFormat.Pdf);
        }

        private void CopyWorksheet(Worksheet inWs, Worksheet outWs)
        {
            //Get the range of cells with all the data from source worksheet
            Aspose.Cells.Range sourceRange = inWs.Cells.MaxDisplayRange;

            //Create a range with same row and column count as source worksheet
            Aspose.Cells.Range destRange = outWs.Cells.CreateRange(sourceRange.FirstRow, sourceRange.FirstColumn, sourceRange.RowCount, sourceRange.ColumnCount);

            //Select Paste Options
            PasteOptions options = new PasteOptions();

            options.PasteType = PasteType.All;

            //Copy the range from source worksheet to destination.
            destRange.Copy(sourceRange, options);
        }

        public void Save()
        {

        }
    }
}
