﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumConverter.Core.models
{
    public class ComponentModel
    {
        public ComponentModel(ComponentRawInfo info, string elementType, string elementName)
        {
            Comment = info.Comment;
            Description = info.Description;
            Footprint = info.Footprint;
            LibRef = info.LibRef;

            GostType = elementType;
            GostName = elementName;
        }

        public ComponentModel(){}

        public string Comment { get; set; }
        public string Description { get; set; }
        public string Footprint { get; set; }
        public string LibRef { get; set; }

        public string GostType { get; set; }
        public string GostName { get; set; }
    }

    public class ComponentGostModel
    {
        public ComponentGostModel(ComponentModel info, string designator, string quanity)
        {
            Comment = info.Comment;
            Description = info.Description;
            Designator = designator;
            Quanity = quanity;
            Footprint = info.Footprint;
            LibRef = info.LibRef;

            GostType = info.GostType;
            GostName = info.GostName;
        }

        public string Comment { get; set; }
        public string Description { get; set; }
        public string Footprint { get; set; }
        public string LibRef { get; set; }

        public string GostType { get; set; }
        public string GostName { get; set; }

        public string Designator { get; set; }
        public string Quanity { get; set; }
    }
}
