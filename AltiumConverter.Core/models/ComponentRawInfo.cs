﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumConverter.Core.models
{
    public class ComponentRawInfo
    {
        public ComponentRawInfo(string comment, string description, string designator, string footprint, string libref, string quanity)
        {
            Comment = string.IsNullOrEmpty(comment) ? "." : comment;
            Description = string.IsNullOrEmpty(description) ? "." : description;
            Designator = string.IsNullOrEmpty(designator) ? "." : designator;
            Footprint = string.IsNullOrEmpty(footprint) ? "." : footprint;
            LibRef = string.IsNullOrEmpty(libref) ? "." : libref;
            Quanity = string.IsNullOrEmpty(quanity) ? "." : quanity;
        }

        public string Comment { get; set; }
        public string Description { get; set; }
        public string Designator { get; set; }
        public string Footprint { get; set; }
        public string LibRef { get; set; }
        public string Quanity { get; set; }

        public override bool Equals(object? obj)
        {
            var info = obj as ComponentRawInfo;
            if (info == null)
            {
                return false;
            }

            return Description == info.Description &&
                   Comment == info.Comment &&
                   Designator == info.Designator &&
                   Footprint == info.Footprint &&
                   LibRef == info.LibRef;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
