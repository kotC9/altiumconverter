﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltiumConverter.Core.models
{
    public enum CommandType
    {
        Input,
        Output,
        Help,
        Unknown
    }



    public class InputArgumentsModel : IParseModel
    {
        public InputArgumentsModel(string inPath = "", string outPath = "")
        {
            InPath = inPath;
            OutPath = outPath;
        }

        public static CommandType ToCommandType(string data) => data switch
        {
            "--input" => CommandType.Input,
            "-i" => CommandType.Input,
            "--output" => CommandType.Output,
            "-o" => CommandType.Output,
            "--help" => CommandType.Help,
            "-h" => CommandType.Help,
            _ => CommandType.Unknown
        };

        public bool TrySetProperty(string command, string content)
        {
            var commandType = ToCommandType(command);
            if (commandType != CommandType.Input && commandType != CommandType.Output)
            {
                return false;
            }

            if (commandType == CommandType.Input)
            {
                InPath = content;
            }
            else if (commandType == CommandType.Output)
            {
                OutPath = content;
            }

            return true;
        }

        public string InPath { get; set; }

        public string OutPath { get; set; }
    }
}
