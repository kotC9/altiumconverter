﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AltiumConverter.Core.models;
using LiteDB;

namespace AltiumConverter.Core.db
{
    internal class DatabaseWorker
    {
        public bool HasInfo(ComponentRawInfo info)
        {
            using var db = new LiteDatabase(@"data/components.db");
            var col = db.GetCollection<ComponentModel>("componentModels");

            return col.Exists(x => x.Description == info.Description &&
                                   x.Comment == info.Comment &&
                                   x.Footprint == info.Footprint &&
                                   x.LibRef == info.LibRef);
        }

        public ComponentModel GetModel(ComponentRawInfo info)
        {
            using var db = new LiteDatabase(@"data/components.db");
            var col = db.GetCollection<ComponentModel>("componentModels");

            return col.FindOne(x => x.Description == info.Description &&
                                    x.Comment == info.Comment &&
                                    x.Footprint == info.Footprint &&
                                    x.LibRef == info.LibRef);
        }

        public List<ComponentModel> GetModels(List<ComponentRawInfo> infos)
        {
            using var db = new LiteDatabase(@"data/components.db");
            var col = db.GetCollection<ComponentModel>("componentModels");

            return infos.Select(info => 
                col.FindOne(x => x.Description == info.Description &&
                                 x.Comment == info.Comment &&
                                 x.Footprint == info.Footprint &&
                                 x.LibRef == info.LibRef)).ToList();
        }

        public void Add(ComponentModel model)
        {
            using var db = new LiteDatabase(@"data/components.db");
            var col = db.GetCollection<ComponentModel>("componentModels");

            col.Insert(model);
        }


        public IEnumerable<ComponentGostModel> GetModelsGost(List<ComponentRawInfo> rawModels)
        {
            using var db = new LiteDatabase(@"data/components.db");
            var col = db.GetCollection<ComponentModel>("componentModels");


            var res = new List<ComponentGostModel>();

            foreach (var info in rawModels)
            {
                var databaseModel = col.FindOne(x => x.Description == info.Description &&
                                 x.Comment == info.Comment &&
                                 x.Footprint == info.Footprint &&
                                 x.LibRef == info.LibRef);

                res.Add(new ComponentGostModel(databaseModel, info.Designator, info.Quanity));
            }

            return res;
        }
    }
}
